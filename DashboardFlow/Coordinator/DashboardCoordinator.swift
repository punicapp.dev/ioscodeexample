
final class DashboardCoordinator: BaseCoordinator, CoordinatorInTabbarInitiable {
    
    // MARK: - Private Properties
    
    private let factory: DashboardModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    private let wireFrame: AlertShowable & ActionSheetShowable = Dependencies.sharedDependencies.wireFrame
    private let api: TabrisApi
    private let profileUseCase: IProfileUseCase
    
    // MARK: - CoordinatorInTabbarInitiable
    
    init(router: Router,
         factory: ModuleFactoryList,
         coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
        self.api = Dependencies.sharedDependencies.api
        self.profileUseCase = ProfileUseCaseImpl(api: api)

    }
    
    // MARK: - Override Methods
    
    override func start() {
        showDashboardModule()
    }
    
    // MARK: - Run current flow's controllers
    
    private func showDashboardModule() {
        var dashboardView = factory.makeDashboardModule()
        let profileUseCase = ProfileUseCaseImpl(api: api)
        dashboardView.viewModel = DashboardViewModel(profileUseCase: profileUseCase, wireframe: wireFrame)
        
        dashboardView.onDetail = { [weak self] (itemType) in
            switch itemType {
        
            case .shopping:
                self?.showShoppingListModule()
            
            case .tabrisPlus:
                self?.showTabrisPlusModule()
                
            case .favoriteDate:
                self?.showFavotireDateModule()
                
            case .questions:
                self?.showFAQModule()
                
            case .feedback:
                print("feedBack")
                self?.wireFrame.showFeedBackActionSheet(onPhoneCall: {
                    UIApplication.shared.makePhoneCall(url: URL.phoneURL)
                }, onWriteMessage: { [weak self] in
                    self?.showFeedbackModule()
                }, in: dashboardView)
                
            case .history:
                self?.showHistoryModule()
            default:
                print("need to open screen: \(itemType)")
            }
        }
        router.setRootModule(dashboardView)
    }
    
    private func showTabrisPlusModule() {
        var tabrisPlusView = factory.makeTabrisPlusModule()
        tabrisPlusView.viewModel = TabrisPlusViewModel(profileUseCase: profileUseCase, wireframe: wireFrame)
        
        configureNavigationView(title: "TabrisPlus.Navigation.Title".localized,
                                style: .backStyle,
                                module: tabrisPlusView)
        
        router.push(tabrisPlusView)
    }
    
    private func showShoppingListModule() {
        let coordinator = coordinatorFactory.makeShoppingCoordinator(router: router)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func showFavotireDateModule() {
        var favoriteView = factory.makeFavoriteDateModule()
        favoriteView.viewModel = FavoriteDateViewModel(profileUseCase: profileUseCase, wireframe: wireFrame)
        
        let title = R.string.localizable.favoriteDateNavigationTitle()
        
        favoriteView.onCheck = { [weak self] in
            self?.router.popModule(animated: true)
        }
        
        _ = NavigationBarHelper.customizeNavBarFor(for: favoriteView,
                                                   style: .backStyleWithCheckButton,
                                                   title: title,
                                                   actionHandler: { [weak self, weak favoriteView] (type) in
                                                    print("navigation button type = \(type)")
                                                    switch type {
                                                    case .back:
                                                        self?.router.popModule(animated: true)
                                                    case .check:
                                                        favoriteView?.checkPressed()
                                                    default: break
                                                    }
        })
        
        router.push(favoriteView, animated: true, hideBottomBar: true, completion: nil)
    }
    
    private func showFAQModule() {
        var faqView = factory.makeFAQModule()
        faqView.viewModel = FAQViewModel(url: URL.faqURL)
        
        configureNavigationView(title: "FAQ.Navigation.Title".localized,
                                style: .backStyle,
                                module: faqView)
        router.push(faqView)
    }

    private func showFeedbackModule() {
        var feedbackView = factory.makeFeedbackModule()
        feedbackView.viewModel = FeedbackViewModel(wireframe: wireFrame)
        
        configureNavigationView(title: "Feedback.Navigation.Title".localized,
                                style: .backStyle,
                                module: feedbackView)
        router.push(feedbackView)
    }

    private func showHistoryModule() {
        var historyView = factory.makeHistoryModule()
        historyView.viewModel = HistoryViewModel(profileUseCase: profileUseCase, wireframe: wireFrame)
        
        configureNavigationView(title: "History.Navigation.Title".localized,
                                style: .backStyleWithStatisticButton,
                                module: historyView)
        router.push(historyView)
        
        historyView.onDetail = { [weak self] (item) in
            self?.showCheckModule(item: item)
        }
    }
    
    private func showCheckModule(item: HistoryOrder) {
        var checkView = factory.makeCheckModule()
        checkView.viewModel = CheckViewModel(historyId: item.id, profileUseCase: profileUseCase, wireframe: wireFrame)
        
        configureNavigationView(title: item.date ?? "",
                                style: .backStyle,
                                module: checkView)
        router.push(checkView)
    }
    
    private func showStatisticsModule() {
        var statisticsView = factory.makeStatisticsModule()
        statisticsView.viewModel = StatisticsViewModel(wireframe: wireFrame)
        
        configureNavigationView(title: "Statistics.Navigation.Title".localized,
                                style: .backStyleWithFilterButton,
                                module: statisticsView)
        router.push(statisticsView)
    }
    
    private func runCartFlow() {
        print("runCartFlow")
        let cartCoordinator = coordinatorFactory.makeCartCoordinator(router: router)
        cartCoordinator.finishFlow = { [weak self, weak cartCoordinator] in
            self?.router.popToRootModule(animated: true)
            self?.removeDependency(cartCoordinator)
        }
        addDependency(cartCoordinator)
        cartCoordinator.start()
    }

    // MARK: - Configuring Navigation View

    private func configureNavigationView(title: String,
                                         style: NavBarStyle = .rootStyle,
                                         module: Presentable) {
        
        _ = NavigationBarHelper.customizeNavBarFor(for: module,
                                                       style: style,
                                                       title: title,
                                                       actionHandler: { [weak self] (type) in
                                                        print("navigation button type = \(type)")
                                                        self?.handleNavigationButtonActionWith(type: type)
            })
        
    }
    
    // MARK: - Navigation Buttons Logic
    
    private func handleNavigationButtonActionWith(type: NavigationButtonType) {
        switch type {
        case .back:
            router.popModule(animated: true)
        
        case .statistic:
            showStatisticsModule()
        
        case .filter:
            print("Need to handle filter button")
        default:
            break
        }
    }
}
