

final class CheckViewController: BaseViewController, CheckViewInput, CheckViewOutput {
    
    // MARK: - CheckViewInput
    
    var viewModel: CheckViewModel!
    
    // MARK: - CheckViewOutput
    
    // MARK: - Outlets
    
    @IBOutlet private weak var totalBottomView: HistoryTotalView!

    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            registerTableViewCell()
        }
    }
    
    // MARK: - Private Properties
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupBindings()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    // MARK: - Public Methods
    
    // MARK: - Private Methods
    
    private func registerTableViewCell() {
        tableView.register(cellInterface: CheckHeaderTableViewCell.self)
        tableView.register(cellInterface: CheckItemTableViewCell.self)
    }
    
    private func setupUI() {
        tableView.applyCommonWhiteDesign()
        totalBottomView.mode = .check
    }
    
    private func setupBindings() {
        let dataSource = self.dataSource()

        viewModel.sections.asObservable()
            .do(onNext: { [weak self] (_) in
                guard let viewModel = self?.viewModel else { return }
                self?.totalBottomView.updateView(totalCount: viewModel.getTotalSum(), points: viewModel.getPoints())
            })
            .bind(to: tableView.rx.items(dataSource: dataSource))
            .disposed(by: disposeBag)
        
        viewModel.executing.subscribe(onNext: { [weak self] (isExecuting) in
            if isExecuting {
                self?.showWaitingScreen()
            } else {
                self?.hideWaitingScreen()
            }
        }).disposed(by: disposeBag)
    }
    
}

// MARK: - RxTableViewSectionedReloadDataSource

extension CheckViewController {
    private func dataSource() -> RxTableViewSectionedReloadDataSource<CheckSectionModel> {
        return RxTableViewSectionedReloadDataSource<CheckSectionModel>(
            configureCell: { (dataSource, tv, indexPath, _) in
                switch dataSource[indexPath] {
                case let .headerCell(cellViewModel: vm):
                    guard let cell = tv.dequeueReusableCell(withIdentifier: CheckHeaderTableViewCell.id, for: indexPath)
                        as? CheckHeaderTableViewCell else {
                            fatalError("Cell is not of kind \(CheckHeaderTableViewCell.nameOfClass)")
                    }
                    cell.configure(vm)
                    return cell
                    
                case let .itemCell(cellViewModel: vm):
                    guard let cell = tv.dequeueReusableCell(withIdentifier: CheckItemTableViewCell.id, for: indexPath)
                        as? CheckItemTableViewCell else {
                            fatalError("Cell is not of kind \(CheckItemTableViewCell.nameOfClass)")
                    }
                    cell.configure(vm)
                    return cell
                }
        })
    }
}
