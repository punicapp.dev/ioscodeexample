

struct CheckHeaderCellViewModel {
    
    let nameTitle: String
    let amountTitle: String
    let priceTitle: String
    let pointsTitle: String

    init() {
        self.nameTitle = "Check.Name.Title".localized
        self.amountTitle = "Check.Amount.Title".localized
        self.priceTitle = "Check.Price.Title".localized
        self.pointsTitle = "Check.Points.Title".localized
    }
}

