

final class CheckHeaderTableViewCell: UITableViewCell, CellInterface {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var containerView: UIView!
   
    @IBOutlet private weak var nameTitleLabel: UILabel!
    @IBOutlet private weak var amountTitleLabel: UILabel!
    @IBOutlet private weak var priceTitleLabel: UILabel!
    @IBOutlet private weak var pointsTitleLabel: UILabel!
    
    // MARK: - Private property
    
    private(set) var disposeBag = DisposeBag()
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameTitleLabel.applyCaption2LabelDesign(color: .labelLightSecondaryColor, alignment: .left)
        amountTitleLabel.applyCaption2LabelDesign(color: .labelLightSecondaryColor, alignment: .right)
        priceTitleLabel.applyCaption2LabelDesign(color: .labelLightSecondaryColor, alignment: .right)
        pointsTitleLabel.applyCaption2LabelDesign(color: .labelLightSecondaryColor, alignment: .right)
    }
    
    // MARK: - Open methods
    
    func configure(_ viewModel: CheckHeaderCellViewModel) {
        nameTitleLabel.text = viewModel.nameTitle
        amountTitleLabel.text = viewModel.amountTitle
        priceTitleLabel.text = viewModel.priceTitle
        pointsTitleLabel.text = viewModel.pointsTitle
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    // MARK: - Private Methods

}

