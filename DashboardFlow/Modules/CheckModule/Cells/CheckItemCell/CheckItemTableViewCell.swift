

final class CheckItemTableViewCell: UITableViewCell, CellInterface {
    
    // MARK: - Outlets
    
    @IBOutlet private weak var containerView: UIView!
   
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var amountLabel: UILabel!
    @IBOutlet private weak var priceLabel: UILabel!
    @IBOutlet private weak var pointsLabel: UILabel!
    
    // MARK: - Private property
    
    private(set) var disposeBag = DisposeBag()
    
    // MARK: - Life cycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nameLabel.applyFootnoteLabelDesign(alignment: .left)
        amountLabel.applyFootnoteLabelDesign(alignment: .right, numberOfLines: 1)
        priceLabel.applyFootnoteLabelDesign(alignment: .right, numberOfLines: 1)
        pointsLabel.applyFootnoteLabelDesign(alignment: .right, numberOfLines: 1)
    }
    
    // MARK: - Open methods
    
    func configure(_ viewModel: CheckItemCellViewModel) {
        nameLabel.text = viewModel.name
        amountLabel.text = viewModel.amount
        priceLabel.text = viewModel.price
        pointsLabel.text = viewModel.points
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
    // MARK: - Private Methods

}
