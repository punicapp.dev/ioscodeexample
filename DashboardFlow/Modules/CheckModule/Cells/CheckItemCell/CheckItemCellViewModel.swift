

struct CheckItemCellViewModel {
    
    let name: String
    let amount: String
    let price: String
    let points: String

    init(product: HistoryProduct) {
        self.name = product.name ?? ""
        self.amount = String(format: "Check.Amount.Formatted.Title".localized, product.quantity ?? 0)
        self.price = product.price?.amount.formatPriceCurrency() ?? 0.0.formatPriceCurrency()
        self.points = String(format: "Check.Points.Formatted.Title".localized, product.bonus ?? 0)
    }
}

