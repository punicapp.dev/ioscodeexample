
enum CheckSectionModel {
    case headerSection(items: [CheckSectionItem])
    case itemsSection(items: [CheckSectionItem])
}

enum CheckSectionItem {
    case headerCell(cellViewModel: CheckHeaderCellViewModel)
    case itemCell(cellViewModel: CheckItemCellViewModel)
}

extension CheckSectionModel: SectionModelType {
    typealias Item = CheckSectionItem
    
    var items: [Item] {
        switch self {
        case let .headerSection(items): return items.map {$0}
        case let .itemsSection(items): return items.map {$0}
        }
    }
    
    init(original: CheckSectionModel, items: [Item]) {
        switch original {
        case let .headerSection(items): self = .headerSection(items: items)
        case let .itemsSection(items): self = .itemsSection(items: items)
        }
    }
}
