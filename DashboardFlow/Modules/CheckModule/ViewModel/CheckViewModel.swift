
final class CheckViewModel {
    
    // MARK: Output
    
    var sections: BehaviorRelay<[CheckSectionModel]> = BehaviorRelay(value: [CheckSectionModel]())
    let executing = BehaviorRelay(value: false)

    // MARK: - Private properties
    
    private let profileUseCase: IProfileUseCase
    private let wireframe: AlertShowable
    private let historyId: Int64
    private var orderDetail: HistoryOrderDetails?

    // MARK: - Initialization
 
    init(historyId: Int64, profileUseCase: IProfileUseCase, wireframe: AlertShowable) {
        self.historyId = historyId
        self.profileUseCase = profileUseCase
        self.wireframe = wireframe
        getDetailOrderInfo()
    }
    
    // MARK: - Private methods
    
    private func configureSections(orderDetail: HistoryOrderDetails) {
        var sectionsItems = [CheckSectionModel]()

        let headerItem = CheckSectionItem.headerCell(cellViewModel: CheckHeaderCellViewModel())
        sectionsItems.append(CheckSectionModel.headerSection(items: [headerItem]))
        
        // items cells
    
        if let products = orderDetail.products {
            sectionsItems.append(CheckSectionModel.itemsSection(items: products.map({ CheckSectionItem.itemCell(cellViewModel: CheckItemCellViewModel(product: $0))})))
        }
        
        sections.accept(sectionsItems)
    }
    
    
    func getDetailOrderInfo() {
        executing.accept(true)
        profileUseCase.getHistoryOrderDetails(id: historyId).subscribe { [weak self] (result) in
            self?.executing.accept(false)
            switch AppResultWrapper(result) {
            case .ok(let orderDetailData):
                print("getHistoryOrderDetails success!")
                if let orderDetail = orderDetailData.data as? HistoryOrderDetails {
                    print("orderDetail = \(orderDetail)")
                    self?.orderDetail = orderDetail
                    self?.configureSections(orderDetail: orderDetail)
                }
                
                
            case .error(let failedRequest):
                self?.wireframe.showFailedRequestAlert(failedRequest)
                let errorMessage = failedRequest.error.message ?? "Error.Connectivity.UndefinedError".localized
                print("getHistoryOrderDetails failedRequest = \(errorMessage)")
            }
        }
    }
    
    // MARK: - Public methods

    func getTotalSum() -> Double {
        guard let order = orderDetail else { return 0.0 }
        let totalSum: Double = order.price?.amount ?? 0.0
        return totalSum
    }
    
    func getPoints() -> Double {
        guard let order = orderDetail else { return 0.0 }
        let pointsSum: Double = (order.bonus as? Double) ?? 0.0
        return pointsSum
    }
}

