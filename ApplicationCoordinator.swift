


private enum LaunchInstructor {
    case main, welcome, onboarding
    
    static func configure(isAuthorized: Bool = false,
                          onboardingWasShown: Bool = onboardingWasShown) -> LaunchInstructor {
        switch (isAuthorized, onboardingWasShown) {
        case (_, false): return .onboarding
        case (false, true): return .welcome
        case (true, true): return .main
        }
    }
}

final class ApplicationCoordinator: BaseCoordinator {
    private let coordinatorFactory: CoordinatorFactory
    private var router: Router
    
    private var instructor: LaunchInstructor {
        return LaunchInstructor.configure(isAuthorized: profileUseCase.isAuthorized())
    }
    
    private let profileUseCase: IProfileUseCase
    
    init(router: Router, coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
        self.profileUseCase = ProfileUseCaseImpl(api: Dependencies.sharedDependencies.api)
    }
    
    override func start() {
        runLoadingFlow()
    }
    
    private func runInstructor() {
        switch instructor {
        case .onboarding: runOnboardingFlow()
        case .welcome: runWelcomeFlow()
        case .main: runMainFlow()
        }
    }
    
    private func runLoadingFlow() {
        let coordinator = coordinatorFactory.makeLoaderCoordinator(router: router)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.runInstructor()
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runOnboardingFlow() {
        let coordinator = coordinatorFactory.makeOnboardingCoordinator(router: router)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            onboardingWasShown = true
            self?.runInstructor()
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runWelcomeFlow() {

        var coordinator = coordinatorFactory.makeWelcomeCoordinator(router: router)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.runInstructor()
            self?.removeDependency(coordinator)
        }
        
        coordinator.onRegistration = { [weak self] in
            self?.runRegistrationFlow()
        }
        
        coordinator.onAuthorization = { [weak self] (authType) in
            self?.runAuthFlow(authType: authType)
        }
        
        coordinator.onGuestMode = { [weak self, weak coordinator] in
            self?.removeDependency(coordinator)
            self?.runMainFlow()
        }
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runAuthFlow(authType: AuthType) {
        let coordinator = coordinatorFactory.makeAuthCoordinator(router: router, authType: authType)
        coordinator.finishFlow = { [weak self] in
            self?.childCoordinators.removeAll()
            self?.runInstructor()
        }
        if var popable = coordinator as? Popable {
            popable.onBack = { [weak self, weak coordinator] in
                self?.removeDependency(coordinator)
            }
        }
        
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runRegistrationFlow() {
        let coordinator = coordinatorFactory.makeRegistCoordinator(router: router)
        coordinator.finishFlow = { [weak self] in
            self?.childCoordinators.removeAll()
            self?.runInstructor()
        }
        
        if var popable = coordinator as? Popable {
            popable.onBack = { [weak self, weak coordinator] in
                self?.removeDependency(coordinator)
            }
        }
        
        addDependency(coordinator)
        coordinator.start()
    }
    
    private func runMainFlow() {
        let (coordinator, module) = coordinatorFactory.makeTabbarCoordinator()
        coordinator.finishFlow = { [weak self, weak coordinator] in
            self?.runInstructor()
            self?.removeDependency(coordinator)
        }
        
        addDependency(coordinator)
        router.setRootModule(module, hideBar: true)
        coordinator.start()
    }
}
