
protocol Popable {
    var onBack: Action? { get set }
}

final class AuthCoordinator: BaseCoordinator, Popable {
    
    // MARK: - Popable
    
    var onBack: Action?
    
    // MARK: - Private Properties
    
    private let factory: AuthModuleFactory
    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    private let api: TabrisApi
    private let wireframe: AlertShowable
    private let authType: AuthType
    
    private let authUseCase: IAuthUseCase
    
    private var authModule: (AuthorizationViewInput & AuthorizationViewOutput )?
    
    init(router: Router, factory: AuthModuleFactory, coordinatorFactory: CoordinatorFactory, authType: AuthType) {
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
        self.router = router
        self.api = Dependencies.sharedDependencies.api
        self.authUseCase = AuthUseCaseImpl(api: api, storage: Dependencies.sharedDependencies.storage)
        self.wireframe = Dependencies.sharedDependencies.wireFrame
        self.authType = authType
    }
    
    override func start() {
        showAuthModule()
    }
    
    // MARK: - Run current flow's controllers
    
    private func showAuthModule() {
        authModule = factory.makeAuthModule()
        authModule?.viewModel = AuthorizationViewModel(authUseCase: authUseCase, wireframe: wireframe, authType: authType)
        router.push(authModule)
        
        authModule?.onSuccessLogin = { [weak self] in
            self?.router.popModule()
            self?.finishFlow?()
        }
        
        authModule?.onRecoveryPassword = { [weak self] in
            self?.showRecoveryPassModule()
        }
        
        authModule?.onBack = { [weak self] in
            self?.router.popModule()
            self?.onBack?()
        }
    }
    
    private func showRecoveryPassModule() {
        var view = factory.makeEnterPhoneModule()
        view.viewModel = EnterPhoneViewModel(mode: .recoveryPassword, authUseCase: authUseCase, wireframe: wireframe)
        view.onNextRecoveryPassword = { [weak self] (smsToken) in
            self?.showPasswordSMSModule(recoveryToken: smsToken)
        }
        router.push(view)
    }
    
    private func showPasswordSMSModule(recoveryToken: String) {
        var view = factory.makeEnterPasswordModule()
        view.viewModel = EnterPasswordViewModel(mode: .recoveryPassword, authUseCase: authUseCase, wireframe: wireframe)
        view.viewModel.recoveryToken = recoveryToken
        view.onSuccessRecoveryPassword = { [weak self] in
            self?.finishFlow?()
        }
        router.push(view)
    }
}
