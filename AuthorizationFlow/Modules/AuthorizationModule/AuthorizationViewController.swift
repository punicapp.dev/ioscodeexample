

final class AuthorizationViewController: BaseViewController, AuthorizationViewInput, AuthorizationViewOutput {

    // MARK: - AuthorizationViewInput
    
    var viewModel: AuthorizationViewModel!

    // MARK: - AuthorizationViewOutput
    
    var onSuccessLogin: Action?
    var onRecoveryPassword: Action?
    var onBack: Action?
    
    // MARK: - Outlets
    
    @IBOutlet private weak var phoneTextField: CustomTextFieldView!
    @IBOutlet private weak var passwordTextField: CustomTextFieldView!
    @IBOutlet private weak var recoveryPasswordButton: UIButton! {
        didSet {
            recoveryPasswordButton.applyTransparentGreenButtonDesign(title: "Auth.RestorePassword.Button.Title".localized)
        }
    }
    
    // MARK: - Private Properties
    
    // MARK: - Life Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
        setupBindings()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if self.isMovingFromParent {
            onBack?()
        }
    }
    
    // MARK: - Actions
    
    @IBAction func recoveryPasswordButtonPressed(_ sender: AnyObject) { onRecoveryPassword?() }
    
    // MARK: - Public Methods
}

private extension AuthorizationViewController {
    
    // MARK: - Private Methods
    
    private func setupUI() {
        self.view.backgroundColor = .commonBackgroundColor
        self.title = "Auth.Navigation.Title".localized
        self.navigationController?.setNavigationBarHidden(false, animated: false)
        applyDesignToBackNavigationButton()
        
        let rightBarButtonItem = UIBarButtonItem(title: "Navigation.NextButton.Title".localized, style: UIBarButtonItem.Style.plain, target: self, action: #selector(nextButtonPressed))
        rightBarButtonItem.tintColor = .primaryColor
        self.navigationItem.rightBarButtonItem = rightBarButtonItem
        
        UIBarButtonItem.applyBarButtonCustomAppearance()

        phoneTextField.setupTextField(with: viewModel.phoneTextFieldTitle,
                                      placeholderText: viewModel.authType == .byCard ?
                                        "Auth.EnterLoyalCard.Placeholder.Title".localized : nil,
                                      hint: nil,
                                      value: "",
                                      secure: false,
                                      dataType: viewModel.authType == .byPhoneNumber ? .phone : .numbers,
                                      isNecessary: false)
        
        passwordTextField.setupTextField(with: "Auth.EnterPassword.Title".localized,
                                         placeholderText: "Auth.EnterPassword.Placeholder.Title".localized,
                                         hint: nil,
                                         value: "",
                                         secure: true,
                                         dataType: .authPassword,
                                         isNecessary: false)
    }
        
    @objc private func nextButtonPressed() {
        print("Next button pressed")
        viewModel.auth { [weak self] in
            self?.onSuccessLogin?()
        }
    }
    
    private func setupBindings() {
        phoneTextField.textBlock = { [weak self] (text) in
            guard let self = self else { return }
            self.viewModel.phone.accept(text)
            self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.isValidFields()
        }
        passwordTextField.textBlock = { [weak self] (text) in
            guard let self = self else { return }
            self.viewModel.password.accept(text)
            self.navigationItem.rightBarButtonItem?.isEnabled = self.viewModel.isValidFields()
        }
        
        viewModel.executing.subscribe(onNext: { [weak self] (isExecuting) in
            if isExecuting {
                self?.showWaitingScreen()
            } else {
                self?.hideWaitingScreen()
            }
        }).disposed(by: disposeBag)
        
        viewModel.phoneErrorMessage.subscribe(onNext: { [weak self] (errorMessage) in
            guard let errorText = errorMessage else { return }
            self?.phoneTextField.showError(errorText)
        }).disposed(by: disposeBag)
        
        viewModel.passwordErrorMessage.subscribe(onNext: { [weak self] (errorMessage) in
            guard let errorText = errorMessage else { return }
            self?.passwordTextField.showError(errorText)
        }).disposed(by: disposeBag)
    }
}
