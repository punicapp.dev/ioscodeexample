
final class AuthorizationViewModel {
    
    // MARK: - Input
    
    // MARK: - Output
    
    let phone = BehaviorRelay(value: "")
    let password = BehaviorRelay(value: "")
    let authType: AuthType
    
    let phoneErrorMessage = BehaviorRelay<String?>(value: nil)
    let passwordErrorMessage = BehaviorRelay<String?>(value: nil)

    
    let executing = BehaviorRelay(value: false)

    // MARK: - Localization
    
    var phoneTextFieldTitle: String {
        switch authType {
        case .byPhoneNumber:
            return "Auth.EnterPhone.Title".localized
        case .byCard:
            return "Auth.EnterLoyalCard.Title".localized
        }
    }
    
    // MARK: - Private properties
    
    private let authUseCase: IAuthUseCase
    private let wireframe: AlertShowable
    private let disposeBag = DisposeBag()
    
    // MARK: - Initialization
    
    init(authUseCase: IAuthUseCase, wireframe: AlertShowable, authType: AuthType) {
        self.authUseCase = authUseCase
        self.wireframe = wireframe
        self.authType = authType
    }
    
    func isValidFields() -> Bool {
        return phone.value.isPhoneNumber && password.value.isAuthPassword ||
            authType == .byCard && !phone.value.isEmpty && password.value.isAuthPassword
    }
    
    func auth(success: Action?) {
        executing.accept(true)
        authUseCase.signIn(login: phone.value, password: password.value).subscribe { [weak self] (result) in
            self?.executing.accept(false)
            switch AppResultWrapper(result) {
            case .ok:
                print("auth success!")
                success?()
            case .error(let failedRequest):

                let throwwable = failedRequest.error
                if let exception = throwwable as? ApiException, let items = exception.dataFieldErrors {
                    print("error items = \(items)")
                    for item in items {
                        switch item.type {
                        case ServerFieldErrorType.login.rawValue:
                            self?.phoneErrorMessage.accept(item.errorText)
                        case ServerFieldErrorType.password.rawValue:
                            self?.passwordErrorMessage.accept(item.errorText)
                        default:
                            break
                        }
                    }
                } else {
                    self?.wireframe.showFailedRequestAlert(failedRequest)
                }
            }
        }
    }
    
}
