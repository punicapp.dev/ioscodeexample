
protocol AuthorizationViewInput {
    var viewModel: AuthorizationViewModel! { get set}
}
