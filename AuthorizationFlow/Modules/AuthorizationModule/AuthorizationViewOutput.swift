
protocol AuthorizationViewOutput: BaseView, NavigationBackButtonObservable {
    var onSuccessLogin: Action? { get set }
    var onRecoveryPassword: Action? { get set }
}
